# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2021-08-26 06:29:34
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2021-08-26 06:30:45
# Project: lending

from lending.settings import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite',
    }
}


SECRET_KEY = 'dsadsadasa'
