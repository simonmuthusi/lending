# Lending Platform

## Overview

See [Lending Design Doc](Lending-Platform.pdf) for an overview of the project and design decisions 

## Setup and Installation

### Requirements 

This project uses 

1. [Python3](https://www.python.org/) - language. Tested with python v3.9
2. [Pip](https://pip.pypa.io/en/stable/installation/) - manage and install python requirements 
3. [PostgresSQL](https://www.postgresql.org/) - database storange
4. [Virtualenv](https://pypi.org/project/virtualenv/) - to manage virtuall environments. Other alternatives can be used

### Installation 

1. Clone this repository: `git clone https://bitbucket.org/simonmuthusi/lending`
2. Cd into lending dicrectory `cd lending`
3. Install [Python3](https://www.python.org/)
4. Install [virtualenv](https://pypi.org/project/virtualenv/)
4. Install [PostgresSQL](https://www.postgresql.org/)
5. Install [Pip](https://pip.pypa.io/en/stable/installation/)
6. Create a environment `virtualenv -p python3 env` and activate `source env/bin/activate`
7. Install requirements `pip install -r requirements-dev.txt`

### Database setup

1. Create database with postgres `psql -U [user]` and create database `create database <your database name>`
2. Copy the file `lending/local.py.sample` to `lending/local.py`
3. Change the database name in the file `lending/local.py` to the database created in step #1. Update other database fields appropriately id using custom settings on Postgres

### Running the app 

1. Run migrations `./manage.py migrate`
2. Create sample users or use step #3. `./manage.py createsuperuser`
3. You can alternatively seed the database with sample data provided in fixtures. Run either step #3 and step #2. To seed the database `./manage.py loaddata fixtures/initial.json`
4. Run the application `./manage.py runserver`

### Login into the app

The app provides two user types: customer and admin. Once you load the fixures these user accounts will be provided to you. You can use them to login

| Username      | User type | Password |
| ----------- | ----------- |----------|
| admin@joinsimon.life| Admin|lend2021|
| ann@joinsimon.life| Customer|lend2021|
| customer2@joinsimon.life| Customer|lend2021|
| customer@joinsimon.life| Customer|lend2021|
| john@joinsimon.life| Customer|lend2021|
| simonmuthusi@gmail.com| Admin|lend2021|

### Running tests 

You can run tests using below ways 
1. Pytest `pytest`
2. Tox `tox`


## Features 

### Ability to login for customer/admin

![User Login](screenshots/login-view.png)

### Customer Dashboard

- View offers, make loan application, view wallet, view notifications
![Customer Dashboard](screenshots/customer-dashboard.png)

### Make loan application/request for an offer

Click on "Request and offer" button on the dashboard
![Request an offer](screenshots/customer-asks-offer.png)

Make application. The interface provides validations 
![Make An aplication](screenshots/customer-asks-offer-2.png)
![Make An aplication 2](screenshots/customer-asks-offer-3-failed.png)

View applied offers
![View applied offers](screenshots/customer-applied-offers)

To cancel a request, click on "Cancel request" action. Can only be performed on applications awaiting approval

### Admin approvals 

Ensure you're logged in with an Admin account. Should give you Admin dashboard 
![Admin dashboard](screenshots/admin-view-requests.png)

To approve/reject a request, click on "Approve offer" button
![Approve offer](screenshots/admin-approves-2.png)
![Reject offer](screenshots/admin-approves-rejects.png)

### View customers 

This is an admin functionality. Click on "Customers" button on the admin dashboard
![View customers](screenshots/admin-view-customers.png)

### Customer view wallet

Click on the "My Wallet" button on the customer dashboard
![Customer wallet](screenshots/customer-wallet.png)

### Customer notifications 

To view notifications, click on "Notifications" button on the customers dashboard
![Customer wallet](screenshots/customer-notifications.png)


