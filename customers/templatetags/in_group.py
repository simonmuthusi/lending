# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2021-08-24 19:44:22
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2021-08-24 20:06:03
# Project: lending

from django import template
from django.utils.encoding import force_text

register = template.Library()


@register.filter(name='in_group')
def in_group(user, groups):
    """
    Validates a user into a given usergroup
    """
    group_list = force_text(groups).split(',')
    return bool(user.groups.filter(name__in=group_list).values('name'))


register.filter('in_group', in_group)
