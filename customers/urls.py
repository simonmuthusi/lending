# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2021-08-24 19:12:06
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2021-08-26 19:54:41
# Project: lending

from django.urls import path

from customers.views import (
    HomeTemplateView, CustomerAcceptOfferTemplateView, CustomerApproveOfferTemplateView,
    CustomerTemplateView, CustomerAskOfferTemplateView, CustomerNotificationTemplateView,
    CustomerCancelOfferTemplateView, CustomerWalletTemplateView
)

urlpatterns = [
    path('', HomeTemplateView.as_view(), name='home'),
    path('customers/',
         CustomerTemplateView.as_view(), name='customer-listing'),
    path('customers/accept-offer/',
         CustomerAcceptOfferTemplateView.as_view(), name='customer-accept-offer'),
    path('customers/approve-offer/',
         CustomerApproveOfferTemplateView.as_view(), name='customer-approve-offer'),
    path('customers/ask-offer/',
         CustomerAskOfferTemplateView.as_view(), name='customer-ask-offer'),
    path('customers/cancel-offer/',
         CustomerCancelOfferTemplateView.as_view(), name='customer-cancel-offer'),
    path('customer/notifications/',
         CustomerNotificationTemplateView.as_view(), name='customer-notification'),
    path('customer/wallet/',
         CustomerWalletTemplateView.as_view(), name='customer-wallet'),


]
