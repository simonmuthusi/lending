# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2021-08-26 06:22:41
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2021-08-27 15:15:41
# Project: lending

import pytest

from datetime import datetime, timedelta

from django.test import RequestFactory
from django.utils import timezone

from mixer.backend.django import mixer

from customers.models import Product, User


class TestModelProduct(object):

    @pytest.mark.django_db
    def test_add_product(self):
        product = mixer.blend('customers.Product',
                              name='1235',
                              max_allowable_limit=5000,
                              interest=10,
                              tenure=15
                              )

        assert product.name == '1235'
        assert product.max_allowable_limit == 5000
        # test __str
        assert str(product) == '1235'
        # test JSON
        assert product.json['name'] == '1235'

    @pytest.mark.django_db
    def test_update_product(self):
        # test update model
        product = mixer.blend('customers.Product',
                              name='1235',
                              max_allowable_limit=5000,
                              interest=10,
                              tenure=15
                              )
        product.name = "New name"
        product.save()

        assert product.name == "New name"


class TestModelUser(object):

    @pytest.mark.django_db
    def test_user(self):
        user = mixer.blend('customers.User',
                           first_name='Simon',
                           last_name='Muthusi',
                           username='simon',
                           email='simon@joinsimon.life',
                           user_type='Customer',
                           phone_number='+254712345678'
                           )

        assert user.first_name == 'Simon'

        # test __str
        assert str(user) == 'simon@joinsimon.life : +254712345678'
        # test short name
        assert user.get_short_name() == 'simon@joinsimon.life'
        # test gull name
        assert user.get_full_name() == 'Simon Muthusi'


class TestModelCustomerOffer(object):

    @pytest.mark.django_db
    def test_user(self):
        # previosly created product & user
        product = mixer.blend('customers.Product',
                              name='New product',
                              max_allowable_limit=5000,
                              interest=10,
                              tenure=15
                              )
        user = mixer.blend('customers.User',
                           first_name='Simon',
                           last_name='Muthusi',
                           username='simon2@joinsimon.life',
                           email='simon2@joinsimon.life',
                           user_type='Customer',
                           phone_number='+254712345679'
                           )

        now = timezone.now()

        offer = mixer.blend('customers.CustomerOffer',
                            product=product,
                            user=user,
                            offer_amount=200,
                            application_date=now
                            )

        # test __str
        assert str(offer) == '{} {}'.format(product, user)

        # testing customer payment
        payment = mixer.blend('customers.CustomerPayment',
                              customer_offer=offer,
                              amount=500,
                              payment_date=now
                              )

        # test __str
        assert str(payment) == '{} {}'.format(offer, now)
