# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2021-08-26 07:11:55
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2021-08-27 16:24:45
# Project: lending

import django
import pytest
import json

from datetime import datetime

from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory
from django.urls import reverse
from django.utils import timezone
from django.test import Client

from mixer.backend.django import mixer

from customers.models import (
    User, Product
)
from customers.views import (
    HomeTemplateView, CustomerTemplateView, CustomerAcceptOfferTemplateView,
    CustomerApproveOfferTemplateView, CustomerAskOfferTemplateView,
    CustomerCancelOfferTemplateView, CustomerWalletTemplateView, CustomerNotificationTemplateView
)


class TestTemplates(object):
    """
    Test all templates combined.
    TODO:: Move to separate classes in future
    """
    client = Client()
    # Test main view loading

    @pytest.mark.django_db
    def test_get_main_view(self):
        request = RequestFactory().get('/')
        request.user = mixer.blend(User)
        response = HomeTemplateView.as_view()(request)

        assert response.status_code == 200
        assert 'Lending Dashboard' in response.context_data['title']

        # test with admin
        request.user = mixer.blend(User, user_type='Admin')
        response = HomeTemplateView.as_view()(request)

        assert response.status_code == 200

    # Test CustomerTemplateView
    @pytest.mark.django_db
    def test_get_customer_template_view(self):
        request = RequestFactory().get('/customers/')
        request.user = mixer.blend(User)
        response = CustomerTemplateView.as_view()(request)

        assert response.status_code == 200
        assert 'Customer Listing' in response.context_data['title']

    # Test CustomerTemplateView GET
    @pytest.mark.django_db
    def test_get_customer_accept_offer_template_view(self):
        request = RequestFactory().get('/customers/accept-offer/',
                                       content_type='application/json')
        request.user = mixer.blend(User)
        response = CustomerAcceptOfferTemplateView.as_view()(request)

        assert response.status_code == 200
        assert 'success' in json.loads(response.content)['message']

    # Test CustomerTemplateView POST
    @pytest.mark.django_db
    def test_post_customer_accept_offer_template_view(self):
        data = {'offer': 333}
        request = RequestFactory().post('/customers/accept-offer/',
                                        data, content_type='application/json')
        request.user = mixer.blend(User)
        response = CustomerAcceptOfferTemplateView.as_view()(request)

        assert response.status_code == 200
        assert False == json.loads(response.content)['status']

    # Test CustomerApproveOfferTemplateView GET
    @pytest.mark.django_db
    def test_get_customer_accept_offer_template_view(self):
        request = RequestFactory().get('/customers/approve-offer/',
                                       content_type='application/json')
        request.user = mixer.blend(User)
        response = CustomerApproveOfferTemplateView.as_view()(request)

        assert response.status_code == 200
        assert 'success' in json.loads(response.content)['message']

    # Test CustomerApproveOfferTemplateView POST
    @pytest.mark.django_db
    def test_post_customer_approve_offer_template_view(self):
        data = {'offer': 333, 'status': 'approved'}
        request = RequestFactory().post('/customers/approve-offer/',
                                        data, content_type='application/json')
        request.user = mixer.blend(User)
        response = CustomerApproveOfferTemplateView.as_view()(request)

        assert response.status_code == 200
        assert False == json.loads(response.content)['status']

    # Test CustomerAskOfferTemplateView GET
    @pytest.mark.django_db
    def test_get_customer_ask_offer_template_view(self):
        request = RequestFactory().get('/customers/ask-offer/')
        request.user = mixer.blend(User)
        response = CustomerAskOfferTemplateView.as_view()(request)

        assert response.status_code == 200

    # Test CustomerAskOfferTemplateView POST
    @pytest.mark.django_db
    def test_post_customer_ask_offer_template_view(self):
        data = {'product': 333, 'amount': 200}
        request = RequestFactory().post('/customers/ask-offer/',
                                        data, content_type='application/json')
        request.user = mixer.blend(User)
        response = CustomerAskOfferTemplateView.as_view()(request)

        assert response.status_code == 200
        assert False == json.loads(response.content)['status']

    # Test CustomerCancelOfferTemplateView GET
    @pytest.mark.django_db
    def test_get_customer_cancel_offer_template_view(self):
        data = {'offer': 1}
        response = self.client.post(
            '/customers/cancel-offer/', data, format='json', follow=True)

        assert response.status_code == 200

        request = RequestFactory().post('/customers/cancel-offer/',
                                        data, content_type='application/json')
        request.user = mixer.blend(User)
        response = CustomerCancelOfferTemplateView.as_view()(request)
        assert response.status_code == 200

    @pytest.mark.django_db
    def test_get_notifications_view(self):
        request = RequestFactory().get('/customer/notifications/')
        request.user = mixer.blend(User)
        response = CustomerNotificationTemplateView.as_view()(request)

        assert response.status_code == 200
        assert 'Customer Notifications' in response.context_data['title']

    @pytest.mark.django_db
    def test_get_wallet_view(self):
        request = RequestFactory().get('/customer/wallet/')
        request.user = mixer.blend(User)
        response = CustomerWalletTemplateView.as_view()(request)

        assert response.status_code == 200
        assert 'Customer Wallet' in response.context_data['title']
