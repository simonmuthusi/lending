# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2021-08-23 23:32:56
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2021-08-27 15:17:01
# Project: lending

import json
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.utils import timezone
from django.db.models import Sum

from customers.models import (
    CustomerOffer, CustomerPayment,
    User, Product, Notification
)
from utils.const import Choice

logger = logging.getLogger(__name__)


class HomeTemplateView(LoginRequiredMixin, TemplateView):
    ''' Home view template '''
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(HomeTemplateView, self).get_context_data(**kwargs)
        context['title'] = 'Lending Dashboard'
        if self.request.user.user_type == 'Customer':
            context['loan_offers'] = CustomerOffer.objects.filter(
                is_active=True, user=self.request.user)
        else:
            context['loan_offers'] = CustomerOffer.objects.filter(
                is_active=True, status__in=[Choice.AWAITING_APPROVAL, Choice.PENDING])

        return context


class CustomerTemplateView(LoginRequiredMixin, TemplateView):
    template_name = "customers/listing.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerTemplateView, self).get_context_data(**kwargs)
        context['title'] = 'Customer Listing'
        context['customers'] = User.objects.filter(
            is_active=True, user_type='Customer')

        return context


class CustomerAcceptOfferTemplateView(LoginRequiredMixin, TemplateView):

    def get(self, request, *args, **kwargs):
        return HttpResponse(json.dumps({'message': 'success'}))

    def post(self, request, *args, **kwargs):
        ''' Accept an offer '''
        offer_id = request.POST.get('offer')
        try:
            offer = CustomerOffer.objects.get(id=offer_id, user=request.user)

            return HttpResponse(json.dumps({
                'status': True,
                'message': 'Request successful. Your reference is {}'.format("c_applied_offer.id")
            }), content_type='application/json')
        except Exception as e:
            logger.error('Error: error on appliying for an offer {}'.format(e), extra={
                'username': request.user}, exc_info=1)
            return HttpResponse(json.dumps({
                'status': False,
                'message': 'Error occured {}'.format(e)
            }), content_type='application/json')


class CustomerApproveOfferTemplateView(LoginRequiredMixin, TemplateView):

    def get(self, request, *args, **kwargs):
        return HttpResponse(json.dumps({'message': 'success'}))

    def post(self, request, *args, **kwargs):
        ''' Approve an offer '''
        offer_id = request.POST.get('offer')
        status = request.POST.get('status')
        try:
            offer = CustomerOffer.objects.get(id=offer_id)

            offer.approved_date = timezone.now()
            offer.approved_by = self.request.user
            offer.status = Choice.ACCEPTED if status == 'approved' else Choice.REJECTED
            offer.save()

            if status == 'approved':
                # If approved, create a payment
                payment, created = CustomerPayment.objects.get_or_create(
                    customer_offer=offer)
                payment.amount = offer.offer_amount
                payment.status = Choice.NOT_PAID
                payment.save()

                # add notification
                notification = Notification.objects.create(
                    user=offer.user,
                    message='Your account has been topped up with {}. Offer ref {} approved'.format(
                        offer.offer_amount, offer.id)
                )
            else:
                notification = Notification.objects.create(
                    user=offer.user,
                    message='Your offer request has been rejected on'.format(
                        offer.approved_date)
                )

            logger.info('Approved {} offer {}'.format(self.request.user, offer), extra={
                        'username': request.user}, exc_info=1)
            return HttpResponse(json.dumps({
                'status': True,
                'message': 'Successfully {} this offer. Your reference is {}'.format(offer.status, offer.id)
            }), content_type='application/json')
        except Exception as e:
            logger.error('Error: error on appliying for an offer {}'.format(e), extra={
                'username': request.user}, exc_info=1)
            return HttpResponse(json.dumps({
                'status': False,
                'message': 'Error occured {}'.format(e)
            }), content_type='application/json')


class CustomerAskOfferTemplateView(LoginRequiredMixin, TemplateView):
    ''' Customer asks for an offer '''
    template_name = "customers/request-offer.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerAskOfferTemplateView,
                        self).get_context_data(**kwargs)
        context['title'] = 'Ask for an offer'
        context['products'] = Product.objects.filter(is_active=True)

        return context

    def post(self, request, *args, **kwargs):
        amount = request.POST.get('amount', 0)
        product_id = request.POST.get('product', 0)
        try:
            product = Product.objects.get(id=product_id)

            # validate amount
            if float(amount) > product.max_allowable_limit:
                raise Exception(
                    'Amount {} specified is not allowed fro this product'.format(amount))

            offer = CustomerOffer.objects.create(
                product=product,
                user=request.user,
                offer_amount=amount,
                application_date=timezone.now(),
                status=Choice.AWAITING_APPROVAL
            )
            return HttpResponse(json.dumps({
                'status': True,
                'message': 'Successfully applied for this offer. Your reference is {}'.format(offer.id)
            }), content_type='application/json')
        except Exception as e:
            return HttpResponse(json.dumps({
                'status': False,
                'message': 'Error occured: {}'.format(e)
            }), content_type='application/json')


class CustomerCancelOfferTemplateView(LoginRequiredMixin, TemplateView):

    def get(self, request, *args, **kwargs):
        return HttpResponse(json.dumps({'message': 'success'}))

    def post(self, request, *args, **kwargs):
        ''' Cancel your own offer '''
        offer_id = request.POST.get('offer')
        try:
            offer = CustomerOffer.objects.get(id=offer_id, user=request.user)
            offer.status = Choice.CANCELLED
            offer.approved_date = timezone.now()
            offer.approved_by = request.user
            offer.save()
            logger.info('Customer cancelled an offer {}'.format(offer), extra={
                        'username': request.user}, exc_info=1)
            return HttpResponse(json.dumps({
                'status': True,
                'message': 'Request successful. You have successfully cancelled this offer'
            }), content_type='application/json')
        except Exception as e:
            logger.error('Error: error on appliying for an offer {}'.format(e), extra={
                'username': request.user}, exc_info=1)
            return HttpResponse(json.dumps({
                'status': False,
                'message': 'Error occured {}'.format(e)
            }), content_type='application/json')


class CustomerNotificationTemplateView(LoginRequiredMixin, TemplateView):
    '''CustomerNotificationTemplateView template '''
    template_name = "customers/notifications.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerNotificationTemplateView,
                        self).get_context_data(**kwargs)
        context['title'] = 'Customer Notifications'
        context['notifications'] = Notification.objects.filter(
            is_active=True, user=self.request.user)

        return context


class CustomerWalletTemplateView(LoginRequiredMixin, TemplateView):
    '''CustomerWalletTemplateView template '''
    template_name = "customers/wallet.html"

    def get_context_data(self, **kwargs):
        context = super(CustomerWalletTemplateView,
                        self).get_context_data(**kwargs)
        payments = CustomerPayment.objects.filter(
            is_active=True, customer_offer__user=self.request.user, status=Choice.PAID)
        accepted_offers = CustomerOffer.objects.filter(
            user=self.request.user, status=Choice.ACCEPTED)
        context['title'] = 'Customer Wallet'
        context['payments'] = payments
        context['accepted_offers'] = accepted_offers

        offer_totals = accepted_offers.aggregate(Sum('offer_amount'))[
            'offer_amount__sum']
        offer_totals = offer_totals if offer_totals else 0

        payment_totals = payments.aggregate(Sum('amount'))['amount__sum']
        payment_totals = payment_totals if payment_totals else 0

        context['balance'] = offer_totals - payment_totals
        return context
