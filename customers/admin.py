# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2021-08-23 23:32:56
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2021-08-26 18:39:56
# Project: lending

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.db.models import JSONField
from django.utils.translation import gettext_lazy as _

from django_json_widget.widgets import JSONEditorWidget

from customers.models import (
    User, Product, CustomerOffer, CustomerPayment
)


class UserAdmin(BaseUserAdmin):

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = (
        'email', 'phone_number', 'first_name',
        'last_name', 'is_staff', 'user_type', 'is_active',
    )
    list_filter = ('user_type', 'email', 'is_active', 'phone_number')
    search_fields = ('id', 'user_type', 'email', 'is_active', 'phone_number')
    ordering = ('email',)
    filter_horizontal = ('groups', 'user_permissions',)
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username', 'email',
                'phone_number', 'password1', 'password2'
            ),
        }),
    )
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {
         'fields': ('first_name', 'last_name', 'email', 'phone_number',
                    )}),
        (_('Demographics'), {'fields': ('user_type',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    formfield_overrides = {
        JSONField: {'widget': JSONEditorWidget},
    }


admin.site.register(User, UserAdmin)
admin.site.register(Product)
admin.site.register(CustomerOffer)
admin.site.register(CustomerPayment)
