# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2021-08-23 23:32:56
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2021-08-27 15:07:36
# Project: lending

from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import JSONField
from django.utils.translation import gettext_lazy as _

from model_utils.models import TimeStampedModel
from phonenumber_field.modelfields import PhoneNumberField

from utils.const import Choice
from utils.managers import CustomUserManager


class User(AbstractUser):
    '''Custom user model.'''

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_number', 'user_type']

    email = models.EmailField(unique=True, null=True,
                              help_text=_('This is also your username'))
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    phone_number = PhoneNumberField(
        _('Phone number (international format)'), unique=True,)
    user_type = models.CharField(max_length=15, choices=Choice.USER_TYPES)

    is_active = models.BooleanField(default=True)

    objects = CustomUserManager()

    def __str__(self):
        return '{} : {}'.format(self.email, self.phone_number)

    def get_full_name(self) -> str:
        return self.first_name + ' ' + self.last_name

    def get_short_name(self):
        return self.email


class Product(TimeStampedModel):
    ''' Products model '''
    name = models.CharField(max_length=255, unique=True)
    max_allowable_limit = models.FloatField(default=0)
    interest = models.FloatField()
    tenure = models.FloatField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @property
    def json(self):
        return {'id': self.id, 'name': self.name, 'max_allowable_limit': self.max_allowable_limit, 'interest': self.interest, 'tenure': self.tenure}


class CustomerOffer(TimeStampedModel):
    ''' CustomerOffers model '''
    product = models.ForeignKey(
        'customers.Product',
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        'customers.User',
        on_delete=models.CASCADE
    )
    offer_amount = models.FloatField()

    application_date = models.DateTimeField()
    approved_date = models.DateTimeField(blank=True, null=True)
    approved_by = models.ForeignKey(
        'customers.User',
        on_delete=models.CASCADE,
        blank=True, null=True,
        related_name='approved_by'
    )
    status = models.CharField(
        max_length=30, choices=Choice.OFFER_STATUSES, default=Choice.AWAITING_APPROVAL)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return '{} {}'.format(self.product, self.user)


class Notification(TimeStampedModel):
    ''' Notification model '''
    user = models.ForeignKey(
        'customers.User',
        on_delete=models.CASCADE
    )
    message = models.TextField()
    is_active = models.BooleanField(default=True)


class CustomerPayment(TimeStampedModel):
    ''' CustomerPayments model '''
    customer_offer = models.OneToOneField(
        'customers.CustomerOffer',
        on_delete=models.CASCADE
    )
    amount = models.FloatField(default=0)
    payment_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(
        max_length=30, choices=Choice.PAYMENT_STATUS, default=Choice.NOT_PAID)
    transaction_details = JSONField(default=dict)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return '{} {}'.format(self.customer_offer, self.payment_date)
