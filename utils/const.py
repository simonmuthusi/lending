# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2021-08-23 23:47:47
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2021-08-26 19:00:08
# Project: lending


class Choice:
    ADMIN = 'Admin'
    CUSTOMER = 'Customer'

    PENDING = 'Pending'
    ACCEPTED = 'Accepted'
    REJECTED = 'Rejected'
    CANCELLED = 'Cancelled'
    AWAITING_APPROVAL = 'Awaiting Approval'

    PAID = 'Paid'
    NOT_PAID = 'Not Paid'

    USER_TYPES = (
        (ADMIN, ADMIN),
        (CUSTOMER, CUSTOMER),
    )

    OFFER_STATUSES = (
        (PENDING, PENDING),
        (ACCEPTED, ACCEPTED),
        (REJECTED, REJECTED),
        (CANCELLED, CANCELLED),
        (AWAITING_APPROVAL, AWAITING_APPROVAL),
    )

    PAYMENT_STATUS = (
        (PAID, PAID),
        (NOT_PAID, NOT_PAID),
    )
