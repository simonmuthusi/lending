/*
* @Author: Simon Muthusi
* @Email: simonmuthusi@gmail.com
* @Date:   2019-01-24 12:16:04
* @Last Modified by:   Simon Muthusi
* @Last Modified time: 2019-05-10 14:36:34
* Project: lab
*/

$(document).ready(function() {

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');
    // approve
$('body').on('click', '.btn-finance-approve', function() {
    var records = $(this).attr("details");
    var request_id = $(this).attr("request_id");
    var text = 'Are you sure you want to approve these ' + records + ' records ?';
    text += '<table class="table table-striped"><thead>';
    text += '<tr><td><strong>Comments</strong></td><td><textarea id="approver_comments" name="comments" rows="4" class="form-control" placeholder="Add comments here"></textarea></td></tr>';
    text += '</table></thead>';
    var clicked = $(this);
    swal({
        title: "Request Approval",
        text: text,
        html: text,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: true,
        closeOnCancel: true,
        closeOnSubmit: false,
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
    }, function(data) {
        if(!data){
            swal.close();
        }

        var approver_comments = $('#approver_comments').val();
        if(!approver_comments.trim()||approver_comments==""){
            $("#approver_comments").addClass("red");
            return false;
        }
        else{
            $("#approver_comments").removeClass("red");
        }

        swal.disableButtons();

        if(data){
            swal.disableButtons();
            // approve requests
            $.ajax({
                url : "/requests/finance/approve/lab-request/",
                type : "post",
                dataType : "json",
                data : {
                    "csrfmiddlewaretoken": getCookie('csrftoken'),
                    "request_id":request_id,
                    "comments":approver_comments
                },
                error : function(xhr, status, error){
                    console.log(error);
                    swal({
                            title: "Oops .. Something went wrong !",
                            text: error,
                            type: "error"
                        }, function(data) { swal.close() });
                },
                success : function (data){
                    if(data['status']){
                        swal.close();
                        dataTable
                        .row( $("#tr"+request_id))
                        .remove()
                        .draw();
                    }else{
                        swal({
                            title: "Oops .. Something went wrong !",
                            text: data['message'],
                            type: "error"
                        }, function(data) { swal.close() });
                    }
                }
            });
        }
        else{
            swal.close();
        }
        clicked.hide();
        $('#btn-finance-reject'+request_id).hide();
        $('#tr'+request_id).hide();
    });
return false;
});
// reject
$('body').on('click', '.btn-finance-reject', function() {
    var records = $(this).attr("details");
    var request_id = $(this).attr("request_id");
    var text = 'Are you sure you want to reject these ' + records + ' records ?';
    text += '<table class="table table-striped"><thead>';
    text += '<tr><td><strong>Reject Reason</strong></td><td><textarea id="reject_comments" name="comments" rows="4" class="form-control" placeholder="Add reject reason here"></textarea></td></tr>';
    text += '</table></thead>';
    var clicked = $(this);
    swal({
        title: "Request Rejection",
        text: text,
        html: text,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: true,
        closeOnCancel: true,
        closeOnSubmit: false,
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
    }, function(data) {

        if(!data){
            swal.close();
        }

        var reject_comments = $('#reject_comments').val();
        if(!reject_comments.trim()||reject_comments==""){
            $("#reject_comments").addClass("red");
            return false;
        }
        else{
            $("#reject_comments").removeClass("red");
        }

        swal.disableButtons();

        // reject requests
        $.ajax({
            url : "/requests/finance/reject/lab-request/",
            type : "post",
            dataType : "json",
            data : {
                "csrfmiddlewaretoken": getCookie('csrftoken'),
                "request_id":request_id,
                "comments":reject_comments
            },
            error : function(xhr, status, error){
                console.log(error);
                swal({
                        title: "Oops .. Something went wrong !",
                        text: error,
                        type: "error"
                    }, function(data) { swal.close() });
            },
            success : function (data){
                if(data['status']){
                    swal.close();
                    dataTable
                    .row( $("#tr"+request_id))
                    .remove()
                    .draw();
                }else{
                    swal({
                        title: "Oops .. Something went wrong !",
                        text: data['message'],
                        type: "error"
                    }, function(data) { swal.close() });
                }
            }
        });
        clicked.hide();
        $('#btn-finance-approve'+request_id).hide();
        $('#tr'+request_id).hide();
    });

return false;
});



// approve
$('body').on('click', '.btn-budget-approve', function() {
    var records = $(this).attr("details");
    var request_id = $(this).attr("request_id");
    var text = 'Are you sure you want to approve these ' + records + ' records ?';
    text += '<table class="table table-striped"><thead>';
    text += '<tr><td><strong>Comments</strong></td><td><textarea id="approver_comments" name="comments" rows="4" class="form-control" placeholder="Add comments here"></textarea></td></tr>';
    text += '</table></thead>';
    var clicked = $(this);
    swal({
        title: "Request Approval",
        text: text,
        html: text,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: true,
        closeOnCancel: true,
        closeOnSubmit: false,
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
    }, function(data) {
        if(!data){
            swal.close();
        }

        var approver_comments = $('#approver_comments').val();
        if(!approver_comments.trim()||approver_comments==""){
            $("#approver_comments").addClass("red");
            return false;
        }
        else{
            $("#approver_comments").removeClass("red");
        }

        swal.disableButtons();

        if(data){
            swal.disableButtons();
            // approve requests
            $.ajax({
                url : "/requests/budget/approve/lab-request/",
                type : "post",
                dataType : "json",
                data : {
                    "csrfmiddlewaretoken": getCookie('csrftoken'),
                    "request_id":request_id,
                    "comments":approver_comments
                },
                error : function(xhr, status, error){
                    console.log(error);
                    swal({
                            title: "Oops .. Something went wrong !",
                            text: error,
                            type: "error"
                        }, function(data) { swal.close() });
                },
                success : function (data){
                    if(data['status']){
                        swal.close();
                        dataTable
                        .row( $("#tr"+request_id))
                        .remove()
                        .draw();
                    }else{
                        swal({
                            title: "Oops .. Something went wrong !",
                            text: data['message'],
                            type: "error"
                        }, function(data) { swal.close() });
                    }
                }
            });
        }
        else{
            swal.close();
        }
        clicked.hide();
        $('#btn-budget-reject'+request_id).hide();
        $('#tr'+request_id).hide();
    });

return false;
});
// reject
$('body').on('click', '.btn-budget-reject', function() {
    var records = $(this).attr("details");
    var request_id = $(this).attr("request_id");
    var text = 'Are you sure you want to reject these ' + records + ' records ?';
    text += '<table class="table table-striped"><thead>';
    text += '<tr><td><strong>Reject Reason</strong></td><td><textarea id="reject_comments" name="comments" rows="4" class="form-control" placeholder="Add reject reason here"></textarea></td></tr>';
    text += '</table></thead>';
    var clicked = $(this);
    swal({
        title: "Request Rejection",
        text: text,
        html: text,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: true,
        closeOnCancel: true,
        closeOnSubmit: false,
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel',
    }, function(data) {

        if(!data){
            swal.close();
        }

        var reject_comments = $('#reject_comments').val();
        if(!reject_comments.trim()||reject_comments==""){
            $("#reject_comments").addClass("red");
            return false;
        }
        else{
            $("#reject_comments").removeClass("red");
        }

        swal.disableButtons();

        // reject requests
        $.ajax({
            url : "/requests/reject/lab-request/",
            type : "post",
            dataType : "json",
            data : {
                "csrfmiddlewaretoken": getCookie('csrftoken'),
                "request_id":request_id,
                "comments":reject_comments
            },
            error : function(xhr, status, error){
                console.log(error);
                swal({
                        title: "Oops .. Something went wrong !",
                        text: error,
                        type: "error"
                    }, function(data) { swal.close() });
            },
            success : function (data){
                if(data['status']){
                    swal.close();
                    dataTable
                    .row( $("#tr"+request_id))
                    .remove()
                    .draw();
                }else{
                    swal({
                        title: "Oops .. Something went wrong !",
                        text: data['message'],
                        type: "error"
                    }, function(data) { swal.close() });
                }
            }
        });
        clicked.hide();
        $('#btn-budget-approve'+request_id).hide();
        $('#tr'+request_id).hide();
    });

return false;
});
});